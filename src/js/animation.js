'use strict';
~ function() {
    var $ = TweenMax,
        tl,
        bgExit = document.getElementById("bgExit"),
        ad = document.getElementById('mainContent');

    window.init = function() {
        bgExit.addEventListener('click', bgExitHandler);


        tl = new TimelineMax({});
        tl.set(ad, { perspective: 1000, force3D: false })
        tl.set(["#copy1","#copy1Returns","#copy1Security","#copy2","#copy3"], { rotation: 0.01,force3D: false  })

        .to('#copy1Returns', 1, { x: 320, rotation: 0.01, ease: Power2.easeInOut }, 2.5)

        .to('.copy1Security', 1, { x: 0, rotation: 0.01, ease: Power2.easeInOut }, 3)
            .to(['.copy1Security', '#copy1'], 1, { opacity: 0, rotation: 0.01, ease: Power2.easeInOut }, 5.5)

        .to('#copy2', 1, { opacity: 1, rotation: 0.01, ease: Power2.easeInOut }, 5.7)
            .to('#copy2', 1, { opacity: 0, rotation: 0.01, ease: Power2.easeInOut }, 9)

        .to('#copy3', 1, { opacity: 1, rotation: 0.01, ease: Power2.easeInOut }, 9.2)
            .to('#copy3', 1, { opacity: 0, rotation: 0.01, ease: Power2.easeInOut }, 11.5)

        .to('#cta', 1, { opacity: 1, rotation: 0.01, ease: Power2.easeInOut }, 11.6)

        .to('#arrow', 1, { opacity: 1, rotation: 0.01, ease: Power2.easeInOut }, 11.6)

        .to('#arrow', .2, { x: 8, yoyo: true, repeat: 1, rotation: 0.01, ease: Power2.easeIn }, 13)
            .to('#arrow', .2, { x: 8, yoyo: true, repeat: 1, rotation: 0.01, ease: Power2.easeIn })

    }

    function bgExitHandler() {
        window.open(window.clickTag);
    }

}();

